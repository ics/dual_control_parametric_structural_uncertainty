clear
close all

% aircraft dynamics
% x(k+1) = A x(k) + gamma^M B u(k) + w(k)

% The state of the system x(k) describes the evolution of the angle of attack, 
% the pitch angle, the pitch rate and the altitude. The input u(k) corresponds to the elevator
% angle, and is constrained to lie between ±0.2 rad. As a simulation scenario, we consider the case in
% which a fault occurs to the actuator some time before the reference change, leading to a decrease in
% gain of 75%. We identify two operating modes of the system with M1 being nominal and M2 being
% the fault mode, each with a corresponding uncertain parameter gamma^M describing the actuator gain

% system matrices
A = [  -1.2822   0    0.98   0;...
    0        0    1      0;...
    -5.4293   0   -1.8366 0;...
    -128.2    128.2  0      0];
B = [-0.3;0;-17;0];

% sampling time
Ts=0.2;

% cost function matrices
Q = diag([0;0;0;1]);
R = 1000;

% dimensions
n_x = size(A,1); % state
n_u = size(B,2); % input
n_m = 2; % number of modes

% true values of gamma per mode
true_gamma = [1;0.25];

% mean and variance of noise w(k)
mW = zeros(n_x,1);
varW = diag([0.0001;0.0001;0.01;0.0001]);
ivarW = inv(varW);

% prior distributions (gamma^M and M)
mu_gamma_M(:,1) = [0.95;0.4];
var_gamma_M(:,1) = [0.001;0.01];
p_M(:,1) = [0.95; 0.05];


% initial state
x(:,1) = zeros(n_x,1);

% time horizon
T = 80;

% control horizon
N = 20;

% fault step
k_fault = 21;

% reference trajectory
delay = 39;
ref = [zeros(k_fault+delay,1); ones(T+N-(k_fault+delay),1)*50];

% number of samples
N_s = 2;

% lookahead steps
L = 1;

% noise samples
w_ = chol(varW)*randn(n_x,T);

simulation = struct;

% gaussian density function
my_mvnpdf = @(x,mu,var) (1/( sqrt((2*pi)^(length(x))*det(var)) ) )*exp( -0.5*(x - mu)'*inv(var)*(x - mu) );

% construct controller with structural + parametric uncertainty
sol = dual_structural_controller(N,N_s,n_m,n_x,n_u,L,A,B,Ts,varW,ivarW,Q,R,my_mvnpdf);


for t=1:T

    for i=1:L
        gamma_M{i} = randn(1,(N_s*n_m)^i);
        w{i} = mW + chol(varW)*randn(n_x,(N_s*n_m)^i);
    end
    wunwrap = [];
    gammaunwrap = [];
    for i=1:L
        wunwrap = [wunwrap, w{i}(:)'];
        gammaunwrap = [gammaunwrap,gamma_M{i}(:)'];
    end
    
    param = [ref(t:t+N-1)', x(:,t)', p_M(:,t)', mu_gamma_M(:,t)',var_gamma_M(:,t)',wunwrap,gammaunwrap];  
    r = sol('x0',0,'lbx',-0.2,'ubx',+0.2,'p',param);

    x_opt = r.x;

    u(:,t) = full(x_opt(1:n_u));

    closedloop_cost(t) = (x(:,t) - [0;0;0;ref(t)])'*Q*(x(:,t) - [0;0;0;ref(t)]) + u(:,t)'*R*u(:,t);
    
    % update closed-loop dynamics based on the mode in which the system is operating
    if t < k_fault
        x(:,t+1) = (A*Ts + eye(n_x))*x(:,t) + Ts*B*true_gamma(1)*u(:,t) + Ts*w_(:,t);

        normlz = 0;
        for model=1:n_m
            x0 = x(:,t+1) - (A*Ts + eye(n_x))*x(:,t);

            var_gamma_M(model,t+1) = 1./( 1./var_gamma_M(model,t) + (Ts*B*u(:,t))'*ivarW*(Ts*B*u(:,t)) );
            mu_gamma_M(model,t+1) = var_gamma_M(model,t+1)*( mu_gamma_M(model,t)./var_gamma_M(model,t) + (Ts*B*u(:,t))'*ivarW*x0 );

            mu = Ts*B*u(:,t)*mu_gamma_M(model,t);
            var = (Ts*B*u(:,t))*(var_gamma_M(model,t))*(Ts*B*u(:,t))' + varW;

            p_M(model,t+1) = my_mvnpdf(x0,mu,var)*p_M(model,t);
            normlz = normlz + p_M(model,t+1);
        end
        p_M(:,t+1) = p_M(:,t+1)/normlz;

    else
        x(:,t+1) = (A*Ts + eye(n_x))*x(:,t) + Ts*B*true_gamma(2)*u(:,t) + Ts*w_(:,t);

        normlz = 0;
        for model=1:n_m
            x0 = x(:,t+1) - (A*Ts + eye(n_x))*x(:,t);

            var_gamma_M(model,t+1) = 1./( 1./var_gamma_M(model,t) + (Ts*B*u(:,t))'*ivarW*(Ts*B*u(:,t)) );
            mu_gamma_M(model,t+1) = var_gamma_M(model,t+1)*( mu_gamma_M(model,t)./var_gamma_M(model,t) + (Ts*B*u(:,t))'*ivarW*x0 );

            mu = Ts*B*u(:,t)*mu_gamma_M(model,t);
            var = (Ts*B*u(:,t))*(var_gamma_M(model,t))*(Ts*B*u(:,t))' + varW;

            p_M(model,t+1) = mvnpdf(x0,mu,var)*p_M(model,t);
            normlz = normlz + p_M(model,t+1);
        end
        p_M(:,t+1) = p_M(:,t+1)/normlz;

    end
    
    % avoiding mode saturation by capping max and min probabilities to 0.95
    % and 0.05, respectively
    p_M(p_M > 0.95) = 0.95;
    p_M(p_M < 0.05) = 0.05;


end

simulation.x = x;
simulation.u = u;
simulation.p_M = p_M;
simulation.closedloop_cost = closedloop_cost;
simulation.w_ = w_;
simulation.mu_gamma_M = mu_gamma_M;
simulation.var_gamma_M = var_gamma_M;





