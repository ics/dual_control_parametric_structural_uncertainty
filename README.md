## Description
Aircraft example using dual control formulation in paper [1]

The MATLAB code relies entrirely on [CasADi](https://web.casadi.org/) and [IPOPT](https://coin-or.github.io/Ipopt/) 

Run `dual_control_structural_example.m` to execute the simulation, consider the number of dual steps and the number of samples per step as tuning knobs. The number of operational modes of the system is problem dependent.


## Authors and acknowledgment
[1] E. Arcari, L. Hewing, M. Schlichting, M. N. Zeilinger, “Dual Stochastic MPC for Systems
with Parametric and Structural Uncertainty”, Learning for Dynamics and Control 2020
